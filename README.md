# OpenML dataset: acute-inflammations

https://www.openml.org/d/1556

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Jacek Czerniak     
**Source**: [original](http://www.openml.org/d/1455) - UCI    
**Please cite**:  J.Czerniak, H.Zarzycki, Application of rough sets in the presumptive diagnosis of urinary system diseases, Artificial Intelligence and Security in Computing Systems, ACS'2002 9th International Conference Proceedings, Kluwer Academic Publishers,2003, pp. 41-51.  

* Abstract: 

The data was created by a medical expert as a data set to test the expert system, which will perform the presumptive diagnosis of two diseases of the urinary system. This is a class-balanced version of acute-inflammations dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1556) of an [OpenML dataset](https://www.openml.org/d/1556). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1556/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1556/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1556/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

